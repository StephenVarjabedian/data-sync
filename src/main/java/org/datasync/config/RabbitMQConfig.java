package org.datasync.config;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@Configuration
public class RabbitMQConfig {

    private final String QUEUE_NAME = "updates";
    private final String ROUTING_KEY = "";
    private final boolean DURABLE = false;
    private final boolean EXCLUSIVE = false;
    private final boolean AUTO_DELETE = false ;
    private final boolean PASSIVE = false;

    @Autowired
    Environment env;

    @Bean
    public ConnectionFactory connectionFactory() {
        return new ConnectionFactory();
    }

    @Bean
    public Connection makeConnection(ConnectionFactory factory) throws IOException, TimeoutException {
        factory.setHost(env.getRequiredProperty("spring.rabbitmq.host"));
        factory.setPort(Integer.parseInt(env.getRequiredProperty("spring.rabbitmq.port")));
        try {
            Connection connection = factory.newConnection();
            return connection;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } catch (TimeoutException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Bean
    public Channel makeChannel(Connection connection) throws IOException {
        try {
            Channel channel = connection.createChannel();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
