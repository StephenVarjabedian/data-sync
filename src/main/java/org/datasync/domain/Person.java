package org.datasync.domain;


public class Person {

    private String emplID;
    private String userId;
    private String empType;
    private String namePrefix;
    private String firstName;
    private String preferredName;
    private String lastName;
    private String jobTitle;
    private String workCity;
    private String workState;
    private String workCountry;
    private String regionId;
    private String salesRegion;

    public Person () {}

    public Person (String emplID, String userId, String empType, String namePrefix, String firstName,
                   String preferredName, String lastName, String jobTitle, String workCity, String workState,
                   String workCountry, String regionId, String salesRegion) {
        this.emplID = emplID;
        this.userId = userId;
        this.empType = empType;
        this.namePrefix = namePrefix;
        this.firstName = firstName;
        this.preferredName = preferredName;
        this.lastName = lastName;
        this.jobTitle = jobTitle;
        this.workCity = workCity;
        this.workState = workState;
        this.workCountry = workCountry;
        this.regionId = regionId;
        this.salesRegion = salesRegion;
    }

    //getters and setters
    public String getEmplID() {
        return emplID;
    }

    public void setEmplID(String emplID) {
        this.emplID = emplID;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmpType() {
        return empType;
    }

    public void setEmpType(String empType) {
        this.empType = empType;
    }

    public String getNamePrefix() {
        return namePrefix;
    }

    public void setNamePrefix(String namePrefix) {
        this.namePrefix = namePrefix;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPreferredName() {
        return preferredName;
    }

    public void setPreferredName(String preferredName) {
        this.preferredName = preferredName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getWorkCity() {
        return workCity;
    }

    public void setWorkCity(String workCity) {
        this.workCity = workCity;
    }

    public String getWorkState() {
        return workState;
    }

    public void setWorkState(String workState) {
        this.workState = workState;
    }

    public String getWorkCountry() {
        return workCountry;
    }

    public void setWorkCountry(String workCountry) {
        this.workCountry = workCountry;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getSalesRegion() {
        return salesRegion;
    }

    public void setSalesRegion(String salesRegion) {
        this.salesRegion = salesRegion;
    }
}
