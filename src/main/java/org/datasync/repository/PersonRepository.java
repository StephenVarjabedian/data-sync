package org.datasync.repository;

import org.datasync.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import javax.xml.transform.Result;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class PersonRepository {

    private String query, testQuery;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    //get updates from HRMS
    public List<Person> loadUpdates(String userId) {

        testQuery = "select EMPLID, USERID, EMP_TYPE, " +
                " NAME_PREFIX, FIRST_NAME, PREFERRED_NAME, LAST_NAME,JOBTITLE," +
                " WORK_CITY, WORK_STATE, WORK_COUNTRY, CS_LGL_REGION_ID, SALES_REGION " +
                " from WF_IDM_SEC_HC_VW " +
                " where USERID = '" + userId + "'";

        query = "select EMPLID, USERID, EMP_TYPE, " +
                " NAME_PREFIX, FIRST_NAME, PREFERRED_NAME, LAST_NAME,JOBTITLE," +
                " WORK_CITY, WORK_STATE, WORK_COUNTRY, CS_LGL_REGION_ID, SALES_REGION " +
                " from WF_IDM_SEC_HC_VW " +
                " where EMPL_STATUS = 'A'" +
                " CONNECT BY PRIOR EMPLID = SUPERVISOR_ID" +
                " START WITH USERID = '" + userId + "'";



        List<Person> people = jdbcTemplate.query(query, new PersonMapper());

        return people;
    }

    private static final class PersonMapper implements RowMapper<Person> {

        public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
            Person person = new Person(
                    rs.getString("EMPLID"),
                    rs.getString("USERID"),
                    rs.getString("EMP_TYPE"),
                    rs.getString("NAME_PREFIX"),
                    rs.getString("FIRST_NAME"),
                    rs.getString("PREFERRED_NAME"),
                    rs.getString("LAST_NAME"),
                    rs.getString("JOBTITLE"),
                    rs.getString("WORK_CITY"),
                    rs.getString("WORK_STATE"),
                    rs.getString("WORK_COUNTRY"),
                    rs.getString("CS_LGL_REGION_ID"),
                    rs.getString("SALES_REGION")
            );

            return person;
        }
    }
}
