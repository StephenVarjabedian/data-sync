package org.datasync.service;

import com.google.gson.Gson;
import com.rabbitmq.client.ConnectionFactory;
import org.datasync.domain.Person;
import org.datasync.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.apache.log4j.Logger;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

@Service("loadService")
public class LoadService {

    static Logger log = Logger.getLogger(LoadService.class.getName());

    //message queue parameters
    //TODO: change HOST to that of the RabbitMQ in production
    private final static String QUEUE_NAME = "hello";
    private final boolean DURABLE = false;
    private final boolean PASSIVE = false;
    private final boolean EXCLUSIVE = false;
    private final boolean AUTO_DELETE = false;

    @Autowired
    Environment env;

    @Autowired
    ConnectionFactory connectionFactory;

    @Autowired
    PersonRepository personRepository;

    public void processUpdates(String userId) {
        log.info("retrieving updates for " + userId + "...");
        List<Person> updates = loadUpdates(userId);
        log.info("Updates successfully retrieved. Sending to message queue...");

        //convert result to json object
        String jsonifiedUpdates = new Gson().toJson(updates);

        sendUpdates(jsonifiedUpdates);
    }

    private List<Person> loadUpdates(String userId) {
        return personRepository.loadUpdates(userId);
    }

    private void sendUpdates(String updatesToSend) {
        try {
            connectionFactory.setHost(env.getRequiredProperty("spring.rabbitmq.host"));
            connectionFactory.setPort(Integer.parseInt(env.getRequiredProperty("spring.rabbitmq.port")));

            log.info("Attempting to create connection with message queue...");
            Connection connection = connectionFactory.newConnection();
            log.info("Connection was successful!");

            Channel channel = connection.createChannel();

            /*
            queueDeclare parameters:
            String queue: queue name
            booelan passive: true if we are passively declaring a queue (asserting the queue already exists)
            boolean durable: true if we are declaring a durable queue (queue will survive a server restart)
            boolean autoDelete: true if we are declaring an autodelete queue (server will delete it when no longer in use)
            Map<Object> arguments: other properties (construction arguments) for queue
             */
            channel.queueDeclare(QUEUE_NAME, DURABLE, AUTO_DELETE, false, null);

            /*
            basicPublish parameters
            String exchange: exchange to publish the message to
            String routingKey: routing key
            BasicProperties props: other properties for message (routing headers, etc)
            byte[] body: message body
             */
            channel.basicPublish("", QUEUE_NAME, null, updatesToSend.getBytes());

            log.info("Sent message: \'" + updatesToSend + "\'");

            channel.close();
            connection.close();
        } catch(TimeoutException e) {
            log.info("Message not sent due to TimeoutException when attempting to make connection to message queue: " + e.toString());
        } catch(Exception e) {
            log.info( "Error sending updates. Error: " + e.toString());
            e.printStackTrace();
        }
    }
}
