package org.datasync.web;

import org.datasync.service.LoadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/load")
public class LoadController {

    @Autowired
    LoadService loadService;

    @RequestMapping("/{id}")
    public void loadUpdates(@PathVariable(value = "id") String userId) {
        //call loadService to handle update
        loadService.processUpdates(userId);
    }
}
