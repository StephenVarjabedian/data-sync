package org.datasync;

import com.rabbitmq.client.*;
import org.apache.log4j.Logger;
import org.datasync.service.LoadService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class DataSyncAppApplicationTests {

	static Logger log = Logger.getLogger(DataSyncAppApplicationTests.class.getName());
	private String[] messages = new String[1];

	//TODO:remove QUEUE_NAME when switching to production message queue credentials
	private final static String QUEUE_NAME = "hello";
    private final boolean autoAck = false;
	private final String QUERY_RESULT_SVARJABE = "{\"items\":[" +
			"{\"emplid\":\"390348\",\"userid\":\"svarjabe\",\"emp_type\":\"R\",\"name_prefix\":\" \"," +
			"\"first_name\":\"Stephen\",\"preferred_name\":\" \",\"last_name\":\"Varjabedian\"," +
			"\"jobtitle\":\"COLLEGE INTERN.TECH UNDERGRAD\",\"work_city\":\"Tucson\",\"work_state\":\"AZ\"," +
			"\"work_country\":\"US\",\"cs_lgl_region_id\":\"USA\",\"sales_region\":\"AMERICAS\"}]}";

	@Autowired
	LoadService loadService;

	@Autowired
	Environment env;

	@Test
	public void testSendUpdates() {

		loadService.processUpdates("svarjabe");
		//assertEquals(1, testNumberOfMessagesSent());

		try {
//			assertEquals(QUERY_RESULT_SVARJABE, configureConsumerForQueue());
            //@TODO - check for key pieces of query result, not exact copy
            assertEquals(QUERY_RESULT_SVARJABE, configureConsumerForQueue());
		} catch (TimeoutException e) {
			log.info("Message not sent due to TimeoutException when attempting to make connection to message queue: " + e.toString());
			e.printStackTrace();
		} catch (Exception e) {
			log.info("Error occurred when trying to extract first message from messageQueue after sending first message.");
			e.printStackTrace();
		}

	}

//	public int testNumberOfMessagesSent() {
//		return 1;
//	}

    // @Todo - Egregious hack. Config should be via Spring and not even exist! ;-)
	private String configureConsumerForQueue() throws IOException, TimeoutException {
		//test sendUpdates()
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(env.getRequiredProperty("spring.rabbitmq.host"));
		factory.setPort(Integer.parseInt(env.getRequiredProperty("spring.rabbitmq.port")));
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		channel.queueDeclare(QUEUE_NAME, false, false, false, null);

//      // We think Consumers are async beasts, based on empirical evidence and the API.
//		Consumer consumer = new DefaultConsumer(channel) {
//			@Override
//			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
//					throws IOException {
//				messages[0] = new String(body, "UTF-8");
//				log.info(" [x] Received '" + messages[0] + "'");
//			}
//		};
//		channel.basicConsume(QUEUE_NAME, autoAck, consumer);

        // Synchronous hack to get a message off the queue - invented only for Unit Tests?
		GetResponse getResponse = channel.basicGet(QUEUE_NAME, autoAck);
		String s = new String(getResponse.getBody());
		System.out.println("getResponse = " + getResponse);
        return s;
	}
}
